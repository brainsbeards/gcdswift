# GCDSwift

[![CI Status](http://img.shields.io/travis/Patryk Peszko/GCDSwift.svg?style=flat)](https://travis-ci.org/Patryk Peszko/GCDSwift)
[![Version](https://img.shields.io/cocoapods/v/GCDSwift.svg?style=flat)](http://cocoapods.org/pods/GCDSwift)
[![License](https://img.shields.io/cocoapods/l/GCDSwift.svg?style=flat)](http://cocoapods.org/pods/GCDSwift)
[![Platform](https://img.shields.io/cocoapods/p/GCDSwift.svg?style=flat)](http://cocoapods.org/pods/GCDSwift)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GCDSwift is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GCDSwift"
```

## Author

Patryk Peszko, patryk@brainsandbeards.com

## License

GCDSwift is available under the MIT license. See the LICENSE file for more info.
