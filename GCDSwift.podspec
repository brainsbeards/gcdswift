#
# Be sure to run `pod lib lint GCDSwift.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "GCDSwift"
  s.version          = "0.1.1"
  s.summary          = "Swift wrapper around C GCD API"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
  			GCD has a pretty ugly interface. It accepts strings which has to be the same
			in all the places. And the worst thing is the flag which has to be 0... always.
			This library takes this problems away and presents itself with nice swift way.
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/GCDSwift"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Patryk Peszko" => "patryk@brainsandbeards.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/GCDSwift.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'GCDSwift' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
