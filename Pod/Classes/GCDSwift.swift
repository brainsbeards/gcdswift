public protocol ExecutableQueue {
    var queue: DispatchQueue { get }
}

public extension ExecutableQueue {
    func executeAsync(_ closure: @escaping () -> Void) {
        queue.async(execute: closure)
    }
}

public enum Queue: ExecutableQueue {
    case main
    case userInteractive
    case userInitiated
    case utility
    case background

    public var queue: DispatchQueue {
        switch self {
        case .main:
            return DispatchQueue.main
        case .userInteractive:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
        case .userInitiated:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
        case .utility:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
        case .background:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        }
    }
}

/**
    This is only to make a point how to define another queues
*/
public enum SerialQueue: String, ExecutableQueue {
    case Custom = "bAnd.SerialQueue.Custom"

    public var queue: DispatchQueue {
        return DispatchQueue(label: rawValue, attributes: [])
    }
}
